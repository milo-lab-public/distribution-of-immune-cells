# Distribution of immune cells



This repository is accompanying the paper ["The Total Mass, Number and Distribution of Immune Cells in the Human Body"]()

Ron Sender, Yarden Weiss, Yoav Navon, Idan Milo, Nofar Azulay, Leeat Keren,
Shai Fuchs, Danny Ben-Zvi, Elad Noor, Ron Milo*

*corresponding aothur: RM ron.milo@weizmann.ac.il


This repository contain six jupyter notebooks several xlsx file.
All data is given in **[`density_data.xlsx`](Data/Literature%20data/density_data.xlsx)|** except for the multiplex data taken from Liu et al. given in their [Zenodo](https://doi.org/10.5281/zenodo.5945388)

The notebooks should be run in the next order:

* **[`multiplex_based_density.ipynb`](./multiplex_based_density.ipynb)|** code extracting the density estimates from Liu et al.'s multplix data. It also produce Figure S1.

* **[`density_calculation.ipynb`](./density_calculations.ipynb)|** code extracting the density estimates taken from the literature and integrating them into estimates for the density and the numbers of cells per cell type and tissue. It also creates comprison with external density and total estimates data based on deconvolution

* **[`cellular_mass_estimate.ipynb`](./cellular_mass_estimate.ipynb)|** code combining the cell type specific cellular mass estimate from the literature. It also produce Figure S6.

* **[`total_uncertainties_calculation.ipynb`](./total_uncertainties_calculation.ipynb)|** code estimating the uncertinaties in the overall estimate, considering possible dependencies. It also create a comparison with data from Tabula Sapiens.

* **[`densities_and_comparison_figures.ipynb`](./densities_and_comparison_figures.ipynb)|** code for generating Figure 1 and supplemental Figures S2-S5, S7-S8

* **[`distribution_figures.ipynb`](./distribution_figures.ipynb)|** code for generating Figures 2-5